from flask import Flask, render_template, request, jsonify
from Controller import generator
import os
import pyrebase

app = Flask(__name__)

path = os.path.dirname(os.path.dirname(__file__))

# Config firebase
config = {
    "apiKey": "AIzaSyBx2PpSEjUuvTVXXNAQjbIrTmDWyLPDm9g",
    'authDomain': "devfest2018-742de.firebaseapp.com",
    'databaseURL': 'https://devfest2018-742de.firebaseio.com/',
    'storageBucket': 'devfest2018-742de.appspot.com',
}

# Ititialize firebase app
firebase = pyrebase.initialize_app(config=config)
db = firebase.database()


@app.route('/')
def hello_world():
    votingName = 'undefined'
    return render_template("search.html")

@app.route('/create', methods=["GET"])
def create_voting():
    return render_template('create-voting.html')

@app.route('/information', methods=["GET"])
def information():
    print(request.args)
    return render_template("voting-info.html", votingName=request.args["name"],votingAddr=request.args["votingaddr"],creator= request.args["creator"])

@app.route('/generateaddr', methods=['GET'])
def initializeAddr():
    if request.method == 'GET':
        return render_template("create-addr.html")

@app.route('/initializeaddr', methods=['GET'])
def get():
    PrivateKey, PublicKey, Address = generator.Initialize()
    print(PrivateKey)
    print(PublicKey)
    print(Address)
    response = {
        "PrivateKey" : PrivateKey,
        "PublicKey" : PublicKey,
        "Address" :Address
            }
    return jsonify(response), 200

@app.route("/createVoting", methods=["POST"])
def createVoting():

    data = request.get_json(force=True)
    print(data)
    db.child("contracts").child("votingName").child(data["votingName"]).set(
        {
            "startDate": data["startDate"],
            "endDate": data["endDate"],
            "votingAddress": data["votingAddress"],
            "isFinish": data["isFinish"],
            "creator" : data["creator"]
        }
    )
    return jsonify({"oke" : 1}), 200

@app.route("/api/research", methods=["POST"])
def reseach():
    data = request.get_json(force=True)
    response = db.child("contracts").child("votingName").child(data["contractName"]).get().val()
    print(response)
    if response is None:
        return jsonify({"result":0})
    return  jsonify(dict(response)), 200

@app.route("/api/resultname", methods=["GET"])
def resultName():
    # print(list(db.child("contracts").child("votingName").get().val().keys()))
    response = {
        "resultName" : list(db.child("contracts").child("votingName").get().val().keys())
    }
    return jsonify(response), 200

@app.route("/api/seed", methods=["GET"])
def intializeSeed():
    response = {
        "seed" : generator.seed()
    }
    return jsonify(response), 200

@app.route("/api/getimage", methods=["POST"])
def getImage():

    data = request.get_json(force=True)
    response = db.child("listBuddy").child(data["id"]+1).get().val()
    print(response)
    return jsonify(dict(response)["image"]), 200

if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=8080, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port
    app.run(host='127.0.0.1', port=8080)
